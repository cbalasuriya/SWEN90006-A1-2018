package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class PartitioningTests
{
  Machine testMachine = new Machine ();


    /** The following tests relate to the relevant EC classes given in the test template tree.
     * The tests shall also be commented to give a brief description of the EC class being tested
     * . */


    // EC1 - testing for no return function in input

    @Test(expected = NoReturnValueException.class)
    public void ec1Test() {

                List<String> list = new ArrayList<String>();
                list.add("MOV R0 5");
                list.add("MOV R1 10");
                list.add("ADD R2 R1 R0");


                testMachine.execute ( list );

    }

    // EC2 - testing for invalid instruction

    @Test(expected = InvalidInstructionException.class)
    public void ec2Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("MOV R1 10");
        list.add("TIM R2 R1 R0");
        list.add("RET R2");

        testMachine.execute ( list );

    }

    // EC3 - ADD function: testing for negative reg assignment

    @Test(expected = InvalidInstructionException.class)
    public void ec3Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("MOV R1 10");
        list.add("ADD R2 R-1 R0");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    // EC4 - ADD function: testing for correct reg assignment

    @Test
    public void ec4Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("MOV R1 10");
        list.add("ADD R2 R1 R0");
        list.add("RET R2");


        int result = testMachine.execute ( list );
        int expected = 15;

        assertEquals ( "ADD function is incorrect", result,expected );

    }

    //EC5 - ADD function: testing for reg assignment greater than 31

    @Test(expected = InvalidInstructionException.class)
    public void ec5Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("ADD R2 R35 R0");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    // EC6 - SUB function: testing for negative reg assignment

    @Test(expected = InvalidInstructionException.class)
    public void ec6Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("MOV R1 10");
        list.add("SUB R2 R-1 R0");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    // EC7 - SUB function: testing for correct reg assignment

    @Test
    public void ec7Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("MOV R1 10");
        list.add("SUB R2 R1 R0");
        list.add("RET R2");


        int result = testMachine.execute ( list );
        int expected = 5;

        assertEquals ( "SUB function is incorrect", result,expected );

    }

    //EC8 - SUB function: testing for reg assignment greater than 31

    @Test(expected = InvalidInstructionException.class)
    public void ec8Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("MOV R1 10");
        list.add("SUB R2 R35 R0");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    // EC9 - MUL function: testing for negative reg assignment

    @Test(expected = InvalidInstructionException.class)
    public void ec9Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("MUL R2 R-1 R0");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    // EC10 - MUL function: testing for correct reg assignment

    @Test
    public void ec10Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 8");
        list.add("MOV R1 10");
        list.add("MUL R2 R1 R0");
        list.add("RET R2");


        int result = testMachine.execute ( list );
        int expected = 80;

        assertEquals ( "MUL function is incorrect", result,expected );

    }

    //EC11 - MUL function: testing for reg assignment greater than 31

    @Test(expected = InvalidInstructionException.class)
    public void ec11Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("MUL R2 R35 R0");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    // EC12 - DIV function: testing for negative reg assignment

    @Test(expected = InvalidInstructionException.class)
    public void ec12Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("DIV R2 R-1 R0");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    // EC13 - DIV function: testing for correct reg assignment

    @Test
    public void ec13Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 2");
        list.add("MOV R1 44");
        list.add("DIV R2 R1 R0");
        list.add("RET R2");


        int result = testMachine.execute ( list );
        int expected = 22;

        assertEquals ( "DIV function is incorrect", result,expected );

    }

    //EC14 - DIV function: testing for reg assignment greater than 31

    @Test(expected = InvalidInstructionException.class)
    public void ec14Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 5");
        list.add("DIV R2 R35 R0");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    // EC15 - RET function: testing for negative reg assignment

    @Test(expected = InvalidInstructionException.class)
    public void ec15Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R-1 5");
        list.add("RET R-1");


        testMachine.execute ( list );

    }

    // EC16 - RET function: testing for correct reg assignment


    public void ec16Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R7 5");
        list.add("RET R7");

        int expected = 5;


        int actual = testMachine.execute ( list );

        assertEquals ( "RET function is incorrect", actual, expected );

    }

    //EC17 - RET function: testing for reg assignment greater than 31

    @Test(expected = InvalidInstructionException.class)
    public void ec17Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R46 32");
        list.add("RET 46");


        testMachine.execute ( list );

    }

    //EC18 - MOV function: testing for reg assignment less than 0

    @Test(expected = InvalidInstructionException.class)
    public void ec18Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R-1 32");
        list.add("RET R-1");


        testMachine.execute ( list );

    }

    //EC19 - MOV function: testing for reg assignment between 0 and 31 and val less than -65535

    @Test(expected = InvalidInstructionException.class)
    public void ec19Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R2 -65536");
        list.add("RET R2");


        testMachine.execute ( list );

    }

    //EC20 - MOV function: testing for reg assignment between 0 and 31 and val in correct range

    @Test
    public void ec20Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R2 36");
        list.add("RET R2");

        int expected = 36;
        int actual = testMachine.execute ( list );

        assertEquals ( "MOV function is incorrect", expected, actual );

    }

    //EC21 - MOV function: testing for reg assignment between 0 and 31 and val greater than 65535

    @Test(expected = InvalidInstructionException.class)
    public void ec21Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R6 65536");
        list.add("RET R6");


        testMachine.execute ( list );

    }

    //EC22 - MOV function: testing for reg assignment greater than 31

    @Test(expected = InvalidInstructionException.class)
    public void ec22Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R36 98");
        list.add("RET R36");


        testMachine.execute ( list );

    }

    //EC23 - JMP function: testing for val < -65535

    @Test(expected = InvalidInstructionException.class)
    public void ec23Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("MOV R1 2");
        list.add("JMP -65537");
        list.add("ADD R6 R5 R2");
        list.add("RET R1");
        list.add("RET R6");


        testMachine.execute ( list );

    }

    //EC24 - JMP function: testing for -65536 <= val <= -1

    @Test
    public void ec24Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("RET R5");
        list.add("JMP -1");
        list.add("ADD R6 R5 R2");
        list.add("RET R1");
        list.add("RET R6");

        int expected = 98;
        int actual = testMachine.execute ( list );

        assertEquals ( "JMP function is incorrect", expected, actual );

    }

    //EC25 - JMP function: testing for val = 0

   /* @Test
    public void ec25Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("JMP 0");
        list.add("ADD R6 R5 R2");
        list.add("RET R1");
        list.add("RET R6");

        testMachine.execute ( list );

    } */


    //EC26 - JMP function: testing for val = 1

    @Test
    public void ec26Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("JMP 1");
        list.add("RET R5");
        list.add("ADD R6 R5 R2");
        list.add("RET R6");

        int expected = 98;


        int actual = testMachine.execute ( list );

        assertEquals ( "JMP 1 has action when it should not", expected, actual );

    }

    //EC27 - JMP function: 2 <= val <= input.length

    @Test
    public void ec27Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("MOV R6 43");
        list.add("JMP 3");
        list.add("RET R5");
        list.add("ADD R6 R5 R2");
        list.add("RET R6");

        int expected = 43;


        int actual = testMachine.execute ( list );

        assertEquals ( "JMP function has incorrect function", expected, actual );

    }

    //EC28 - JMP function: input.length <= val < 65535

    @Test(expected = NoReturnValueException.class)
    public void ec28Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("MOV R6 43");
        list.add("JMP 4");
        list.add("RET R5");
        list.add("ADD R6 R5 R2");
        list.add("RET R6");


        testMachine.execute ( list );


    }

    //EC29 - JMP function: val > 65535

    @Test(expected = InvalidInstructionException.class)
    public void ec29Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("MOV R6 43");
        list.add("JMP 65536");


        testMachine.execute ( list );


    }

    //EC30 - JZ function:  reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void ec30Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("ADD R3 R2 R5");
        list.add("JZ R-1 65536");
        list.add("RET R3");


        testMachine.execute ( list );


    }

    //EC31 - JZ function:  reg in valid range, but val < -65535

    @Test(expected = InvalidInstructionException.class)
    public void ec31Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("ADD R3 R2 R5");
        list.add("JZ R1 -65536");
        list.add("RET R3");


        testMachine.execute ( list );


    }

    //EC32 - JZ function:  reg in valid range and val in valid range but reg not registered to 0

    @Test
    public void ec32Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("ADD R3 R2 R5");
        list.add("MOV R1 32");
        list.add("JZ R1 5");
        list.add("RET R3");
        list.add("RET R5");


        int expected = 100;
        int actual = testMachine.execute ( list );

        assertEquals ( "JZ function has incorrect function when reg value is not 0" ,expected,
                actual );

    }

    //EC33 - JZ function:  reg in valid range and val in valid range and reg  registered to 0

    @Test
    public void ec33Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("ADD R3 R2 R5");
        list.add("MOV R1 0");
        list.add("JZ R1 2");
        list.add("RET R3");
        list.add("RET R5");


        int expected = 98;
        int actual = testMachine.execute ( list );

        assertEquals ( "JZ function has incorrect function when reg value is 0", expected,
                actual );

    }

    //EC34 - JZ function:  reg in valid range but val > 65535

    @Test(expected = InvalidInstructionException.class)
    public void ec34Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("ADD R3 R2 R5");
        list.add("MOV R1 0");
        list.add("JZ R1 65536");
        list.add("RET R3");
        list.add("RET R5");



        testMachine.execute ( list );



    }

    //EC35 - JZ function:  reg > 31

    @Test(expected = InvalidInstructionException.class)
    public void ec35Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R5 98");
        list.add("MOV R2 2");
        list.add("ADD R3 R2 R5");
        list.add("MOV R1 0");
        list.add("JZ R36 655");
        list.add("RET R3");
        list.add("RET R5");

        testMachine.execute ( list );

    }

    //EC36 - STR function:  reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void ec36Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 14");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R-2");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

        testMachine.execute ( list );



    }

    //EC37 - STR function:  reg is valid but val < -65536

    @Test(expected = InvalidInstructionException.class)
    public void ec37Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 14");
        list.add("MOV R3 20" );
        list.add("STR R1 -65537 R2");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

        testMachine.execute ( list );


    }

    //EC38 - STR function:  reg is valid and val is valid

    @Test
    public void ec38Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 14");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R2");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

        int expected = 14;
        int actual = testMachine.execute ( list );

        assertEquals ( "STR function is incorrect", expected,actual );

    }

    //EC39 - STR function:  reg is valid, but val > 65535

    @Test(expected = InvalidInstructionException.class)
    public void ec39Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 14");
        list.add("MOV R3 20" );
        list.add("STR R1 65536 R2");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

        testMachine.execute ( list );

    }

    //EC40 - STR function:  reg > 31

    @Test(expected = InvalidInstructionException.class)
    public void ec40Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 14");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R38");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

        testMachine.execute ( list );



    }

    //EC41 - LDR function:  reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void ec41Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 14");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R-2");
        list.add("LDR R-4 R3 80");
        list.add("RET R4");

        testMachine.execute ( list );


    }

    //EC42 - LDR function:  reg in range, but val < -65535

    @Test(expected = InvalidInstructionException.class)
    public void ec42Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 14");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R2");
        list.add("LDR R4 R3 -65536");
        list.add("RET R4");

        testMachine.execute ( list );


    }

    //EC43 - LDR function:  reg in range, val in range but memory location previously not defined

    @Test
    public void ec43Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 14");
        list.add("MOV R3 25" );
        list.add("STR R1 98 R2");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

        int expected = 0;

        int actual = testMachine.execute ( list );

        assertEquals ( "LDR function accessing memory incorrectly", expected, actual);


    }

    //EC44 - LDR function:  reg in range, val in range and memory location previously  defined

    @Test
    public void ec44Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 24");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R2");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

        int expected = 24;

        int actual = testMachine.execute ( list );

        assertEquals ( "LDR function accessing memory incorrectly", expected, actual);


    }

    //EC45 - LDR function:  reg in range, val > 65535


    @Test(expected = InvalidInstructionException.class)
    public void ec45Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 24");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R2");
        list.add("LDR R4 R3 653390");
        list.add("RET R4");

        testMachine.execute ( list );

    }

    //EC46 - LDR function:  reg > 31

    @Test(expected = InvalidInstructionException.class)
    public void ec46Test() {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 24");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R2");
        list.add("LDR R40 R3 12");
        list.add("RET R4");

        testMachine.execute ( list );

    }
}
