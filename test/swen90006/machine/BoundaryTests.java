package swen90006.machine;

import java.util.List;
import java.util.ArrayList;
import java.nio.charset.Charset;
import java.nio.file.Path;
import java.nio.file.Files;
import java.nio.file.FileSystems;

import org.junit.*;
import static org.junit.Assert.*;

public class BoundaryTests
{
  Machine testMachine = new Machine ();
 

  //Any method annotation with "@Test" is executed as a test.

    /** The following represent the tests for the boundary value analysis. Note that
     * all the tests are present in the suite, where multiple tests are used to test the
     * boundaries between equivalence classes. A description shall be present before each test
     * to denote what is being tested and between which equivalence classes */


  // BVA Test1 - between EC3-4 - ADD instruction - on point for Reg < 0

  @Test public void bvaTest1 ()
  {

      List<String> list = new ArrayList<String>();
      list.add("MOV R1 7");
      list.add("MOV R2 10");
      list.add("ADD R0 R1 R2");
      list.add("RET R0");

      int expected = 17;


      int actual = testMachine.execute ( list );

      assertEquals("Incorrect boundary for reg in ADD instruction",expected, actual);

  }

    // BVA Test2 - between EC3-4 - ADD instruction - off point for Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest2 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 7");
        list.add("MOV R2 10");
        list.add("ADD R-1 R1 R2");
        list.add("RET R-1");

        testMachine.execute ( list );


    }

    // BVA Test3 - between EC4-5 - ADD instruction - on point for Reg > 31

    @Test public void bvaTest3 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 7");
        list.add("MOV R2 154");
        list.add("ADD R31 R1 R2");
        list.add("RET R31");

        int expected = 161;

        int actual = testMachine.execute ( list );

        assertEquals("Incorrect boundary for reg in ADD instruction", expected, actual);

    }

    // BVA Test4 - between EC4-5 - ADD instruction - off point for Reg > 31

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest4 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 27");
        list.add("MOV R2 10");
        list.add("ADD R32 R1 R2");
        list.add("RET R32");

        testMachine.execute ( list );


    }

    // BVA Test5 - between EC6-7 - SUB instruction -  on point for Reg < 0

    @Test public void bvaTest5 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 7");
        list.add("MOV R2 10");
        list.add("SUB R0 R1 R2");
        list.add("RET R0");

        int expected = -3;

        int actual = testMachine.execute ( list );

        assertEquals("Incorrect boundary for reg in SUB instruction", expected, actual);

    }

    // BVA Test6 - between EC6-7 - SUB instruction -  off point for Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest6 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 7");
        list.add("MOV R2 10");
        list.add("SUB R-5 R1 R2");
        list.add("RET R-5");

        testMachine.execute ( list );

    }

    // BVA Test7 - between EC7-8 - SUB instruction -  on point for Reg > 31

    @Test
    public void bvaTest7 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 17");
        list.add("MOV R2 10");
        list.add("SUB R31 R1 R2");
        list.add("RET R31");

        int expected = 7;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in SUB instruction", expected, actual );

    }

    // BVA Test8 - between EC7-8 - SUB instruction - off point for Reg > 31

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest8 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 6");
        list.add("MOV R2 10");
        list.add("SUB R32 R1 R2");
        list.add("RET R32");

        testMachine.execute ( list );
    }

    // BVA Test9 - between EC9-10 - MUL instruction - on point for Reg < 0

    @Test
    public void bvaTest9 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 17");
        list.add("MOV R2 10");
        list.add("MUL R0 R1 R2");
        list.add("RET R0");

        int expected = 170;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in MUL instruction", expected, actual );

    }

    // BVA Test10 - between EC9-10 - MUL instruction - off point for Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest10 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 6");
        list.add("MOV R2 143");
        list.add("MUL R-1 R1 R2");
        list.add("RET R-1");

        testMachine.execute ( list );
    }

    // BVA Test11 - between EC10-11 - MUL instruction - on point for Reg > 31

    @Test
    public void bvaTest11 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 16");
        list.add("MOV R2 10");
        list.add("MUL R31 R1 R2");
        list.add("RET R31");

        int expected = 160;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in MUL instruction", expected, actual );

    }

    // BVA Test12 - between EC10-11 - MUL instruction - off point for Reg > 31

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest12 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 6");
        list.add("MOV R2 100");
        list.add("MUL R32 R1 R2");
        list.add("RET R32");

        testMachine.execute ( list );
    }

    // BVA Test13 - between EC12-13 - DIV instruction - on point for Reg < 0

    @Test
    public void bvaTest13 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 16");
        list.add("MOV R2 4");
        list.add("DIV R0 R1 R2");
        list.add("RET R0");

        int expected = 4;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in DIV instruction",  expected, actual );

    }

    // BVA Test14 - between EC12-13 - DIV instruction - off point for Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest14 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 6");
        list.add("MOV R2 100");
        list.add("DIV R-1 R1 R2");
        list.add("RET R-1");

        testMachine.execute ( list );
    }

    // BVA Test15 - between EC13-14 - DIV instruction - on point for Reg > 31

    @Test
    public void bvaTest15 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 24");
        list.add("MOV R2 4");
        list.add("DIV R31 R1 R2");
        list.add("RET R31");

        int expected = 6;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in DIV instruction", expected, actual );

    }
    // BVA Test16 - between EC13-14 - DIV instruction - off point for Reg > 31

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest16 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 6");
        list.add("MOV R2 100");
        list.add("DIV R32 R1 R2");
        list.add("RET R32");

        testMachine.execute ( list );
    }

    // BVA Test17 - between EC15-16 - RET instruction - on point for Reg < 0

    @Test
    public void bvaTest17 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 24");
        list.add("RET R1");

        int expected = 24;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in RET instruction", expected, actual );

    }
    // BVA Test18 - between EC15-16 - RET instruction - off point for Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest18 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R-1 6");
        list.add("RET R-1");

        testMachine.execute ( list );
    }

    // BVA Test19 - between EC16-17 - RET instruction - on point for Reg > 31

    @Test
    public void bvaTest19 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R31 54");
        list.add("RET R31");

        int expected = 54;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in RET instruction", expected, actual );

    }
    // BVA Test20 - between EC16-17 - RET instruction - off point for Reg > 31

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest20 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R32 6");
        list.add("RET 32");

        testMachine.execute ( list );
    }

    // BVA Test21 - between EC18 - MOV instruction - on point for Reg < 0

    @Test
    public void bvaTest21 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 54");
        list.add("RET R0");

        int expected = 54;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in MOV instruction", expected, actual );

    }
    // BVA Test22 - between EC18 - MOV instruction - off point for Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest22 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R-1 6");
        list.add("RET R-1");

        testMachine.execute ( list );
    }

    // BVA Test23 - between EC19-20 - MOV instruction - on point for Reg < 31
    // on point for val < -65535

    @Test
    public void bvaTest23 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R31 -65535");
        list.add("RET R31");

        int expected = -65535;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in MOV instruction", expected, actual );

    }
    // BVA Test24 - between EC19-20 - MOV instruction - off point for Reg < 31
    // off point for val < -65536

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest24 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R32 -65536");
        list.add("RET R32");

        testMachine.execute ( list );
    }

    // BVA Test25 - between EC20-21 - MOV instruction - on point for Reg < 31
    // on point for val < 65535

    @Test
    public void bvaTest25 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R31 65535");
        list.add("RET R31");

        int expected = 65535;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in MOV instruction", expected, actual );

    }

    // BVA Test26 - between EC20-21 - MOV instruction - off point for Reg < 31
    // off point for val < 65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest26 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R32 65536");
        list.add("RET R31");

        testMachine.execute ( list );
    }

    // BVA Test27 - between EC23-24 - JMP instruction - off point for val < -65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest27 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("JMP -65536");


        testMachine.execute ( list );
    }

    // BVA Test28 - between EC23-24 - JMP instruction - on point for val < -65535

    @Test(expected = NoReturnValueException.class)
    public void bvaTest28 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 4");
        list.add("JMP -65535");
        list.add("RET R1");


        testMachine.execute ( list );
    }

    // BVA Test29 - between EC24-25 - JMP instruction - off point for val < -1

    @Test(expected = NoReturnValueException.class)
    public void bvaTest29 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 65");
        list.add("JMP -2");
        list.add("RET R1");


        testMachine.execute ( list );
    }

    // BVA Test30 - between EC24-25 - JMP instruction - on point for val < -1

    @Test(expected = NoReturnValueException.class)
    public void bvaTest30 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("JMP -1");
        list.add("RET R1");


        testMachine.execute ( list );
    }



    // BVA Test31 - between EC25-26 - JMP instruction - on point for val = 0

    /*@Test(expected = NoReturnValueException.class)
    public void bvaTest31 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("JMP 0");
        list.add("RET R1");


        testMachine.execute ( list );
    } */

    // BVA Test32 - between EC24-25 - JMP instruction - on point for val = 1

    @Test
    public void bvaTest32 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 5556");
        list.add("MOV R2 543");
        list.add("JMP 1");
        list.add("RET R1");
        list.add("ADD R3 R1 R2");

        int actual = 5556;
        int expected = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for val=1 in JMP instruction", actual, expected);
    }

    // BVA Test33 - between EC26-27 - JMP instruction - on point for val < input.length

    @Test
    public void bvaTest33 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("JMP 4");
        list.add("MOV R2 543");
        list.add("RET R1");
        list.add("ADD R3 R1 R2");
        list.add("RET R4");

        int actual = 0;
        int expected = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for val in JMP instruction", actual, expected);
    }

    // BVA Test34 - between EC27-28 - JMP instruction - off point for val < input.length

    @Test(expected = NoReturnValueException.class)
    public void bvaTest34 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("JMP 5");
        list.add("MOV R2 543");
        list.add("RET R1");
        list.add("ADD R3 R1 R2");
        list.add("RET R4");

        int actual = 0;
        int expected = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for val in JMP instruction", actual, expected);
    }

    // BVA Test35 - between EC28-29 - JMP instruction - on point for val > 65535

    @Test(expected = NoReturnValueException.class)
    public void bvaTest35 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("JMP 65535");
        list.add("RET R4");

        int actual = 0;
        int expected = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for val in JMP instruction", actual, expected);
    }

    // BVA Test36 - between EC28-29 - JMP instruction - off point for val > 65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest36 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("JMP 65536");
        list.add("RET R4");

        int actual = 0;
        int expected = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for val in JMP instruction", actual, expected);
    }

    // BVA Test37 - between EC30 - JZ instruction - on point Reg < 0

    @Test
    public void bvaTest37 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("JZ R1 3");
        list.add("RET R1");

        int actual = 2;
        int expected = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in JZ instruction", actual, expected);
    }

    // BVA Test38 - between EC30 - JZ instruction - off point Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest38 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R-1 2");
        list.add("JZ R-1 3");
        list.add("RET R2");

        int actual = 2;
        int expected = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in JZ instruction", actual, expected);
    }

    // BVA Test39 - between EC31 - JZ instruction - on point Reg < 0
    // and on point val < -65535

    @Test
    public void bvaTest39 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R31 65");
        list.add("JZ R31 -65535");
        list.add("RET R31");

        int actual = 65;
        int expected = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in JZ instruction", actual, expected);
    }

    // BVA Test40 - between EC31 - JZ instruction - off point Reg < 0
    // and off point val < -65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest40 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R-1 65");
        list.add("JZ R-1 -65536");
        list.add("RET R31");

        testMachine.execute ( list );


    }

    // BVA Test41 - between EC33-34 - JZ instruction - on point Reg < 31
    // and on point val < 65535 and on point Reg holds 0

    @Test(expected = NoReturnValueException.class)
    public void bvaTest41 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R3 0");
        list.add("MOV R4 4");
        list.add("JZ R3 65535");
        list.add("RET R4");

        testMachine.execute ( list );

    }

    // BVA Test42 - between EC33-34 - JZ instruction - on point Reg < 31
    // and on point val < 65535 and off point Reg holds 0

    @Test
    public void bvaTest42 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R3 -1");
        list.add("MOV R4 4");
        list.add("JZ R3 65535");
        list.add("RET R4");

        int expected = 4;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in JZ instruction",actual, expected );

    }

    // BVA Test43 - between EC34-35 - JZ instruction - on point Reg < 31
    // and on point val < 65535

    @Test
    public void bvaTest43 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R3 1");
        list.add("MOV R4 4");
        list.add("JZ R3 65535");
        list.add("RET R4");

        int expected = 4;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in JZ instruction",actual, expected );

    }

    // BVA Test44 - between EC34-35 - JZ instruction - off point Reg < 31
    // and off point val < 65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest44 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R3 1");
        list.add("MOV R4 6");
        list.add("JZ R32 65536");
        list.add("RET R6");

        int expected = 6;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in JZ instruction", actual, expected );

    }

    // BVA Test45 - between EC36 - STR instruction - on point Reg < 0

    @Test
    public void bvaTest45 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 16");
        list.add("MOV R3 20" );
        list.add("STR R1 98 R2");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

        int expected = 16;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in STR instruction", expected,actual );

    }

    // BVA Test46 - between EC36 - STR instruction - off point Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest46 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 16");
        list.add("MOV R3 20" );
        list.add("STR R-1 98 R2");
        list.add("LDR R4 R3 80");
        list.add("RET R4");

       testMachine.execute ( list );

    }

    // BVA Test47 - between EC37-38 - STR instruction - on point Reg < 31 and
    // on point val < -65535

    @Test
    public void bvaTest47 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 0");
        list.add("MOV R2 45");
        list.add("MOV R3 -1" );
        list.add("STR R1 -65535 R2");
        list.add("LDR R4 R3 -65534");
        list.add("RET R4");

        int expected = 0;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in STR instruction", expected, actual);

    }

    // BVA Test48 - between EC37-38 - STR instruction - off point Reg < 31 and
    // off point val < 65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest48 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 1");
        list.add("MOV R2 65535");
        list.add("MOV R3 6" );
        list.add("STR R1 -65536 R32");
        list.add("LDR R4 R3 65530");
        list.add("RET R4");

        testMachine.execute ( list );


    }

    // BVA Test49 - between EC38-39 - STR instruction - on point Reg < 31 and
    // on point val < 65535

    @Test
    public void bvaTest49 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R1 2");
        list.add("MOV R2 655");
        list.add("MOV R3 5" );
        list.add("STR R1 65533 R2");
        list.add("LDR R4 R3 65530");
        list.add("RET R4");

        int expected = 655;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in STR instruction", expected, actual);

    }

    // BVA Test50 - between EC38-39 - STR instruction - off point Reg < 31 and
    // off point val < 65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest50 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 2");
        list.add("MOV R2 655");
        list.add("MOV R3 5" );
        list.add("STR R32 65536 R2");
        list.add("LDR R4 R3 65530");
        list.add("RET R4");

        testMachine.execute ( list );

    }

    // BVA Test51 - between EC41 - LDR instruction - off point Reg < 0

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest51 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R0 2");
        list.add("MOV R2 655");
        list.add("MOV R3 5" );
        list.add("STR R0 65536 R2");
        list.add("LDR R-4 R3 65530");
        list.add("RET R-4");

        testMachine.execute ( list );

    }

    // BVA Test52 - between EC41 - LDR instruction - on point Reg < 0

    @Test
    public void bvaTest52 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R30 0");
        list.add("MOV R2 600");
        list.add("MOV R3 5" );
        list.add("STR R30 5 R2");
        list.add("LDR R0 R3 0");
        list.add("RET R0");

        int expected = 600;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg in LDR instruction", expected, actual );

    }

    // BVA Test53 - between EC42 - LDR instruction - on point Reg < 31 and
    // val < -65535

    @Test
    public void bvaTest53 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R30 0");
        list.add("MOV R2 600");
        list.add("MOV R3 0" );
        list.add("STR R30 -65535 R2");
        list.add("LDR R0 R3 -65535");
        list.add("RET R0");

        int expected = 0;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in LDR instruction", expected, actual );

    }

    // BVA Test54 - between EC42 - LDR instruction - off point Reg < 31 and
    // val < -65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest54 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R30 0");
        list.add("MOV R2 600");
        list.add("MOV R3 0" );
        list.add("STR R3 -65535 R2");
        list.add("LDR R32 R3 -65536");
        list.add("RET R0");

        int expected = 0;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in LDR instruction", expected, actual );

    }

    // BVA Test55 - between EC43-45 - LDR instruction - on point Reg < 31 and
    // val < 65535

    @Test
    public void bvaTest55 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R30 0");
        list.add("MOV R2 600");
        list.add("MOV R3 0" );
        list.add("STR R3 65535 R2");
        list.add("LDR R31 R3 65535");
        list.add("RET R31");

        int expected = 600;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in LDR instruction", expected, actual );

    }

    // BVA Test56 - between EC43-45 - LDR instruction - off point Reg < 31 and
    // val < 65535

    @Test(expected = InvalidInstructionException.class)
    public void bvaTest56 ()
    {

        List<String> list = new ArrayList<String>();
        list.add("MOV R30 0");
        list.add("MOV R2 600");
        list.add("MOV R3 0" );
        list.add("STR R3 65535 R2");
        list.add("LDR R32 R1 65536");
        list.add("RET R32");

        int expected = 0;
        int actual = testMachine.execute ( list );

        assertEquals ( "Incorrect boundary for reg and val in LDR instruction", expected, actual );

    }
  
}
